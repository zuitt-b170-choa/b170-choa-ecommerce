const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	sellerDescription:[
	{
		sellerId:{
			type: String,
			required: [true, "Seller ID is required."]
		},
		sellerAddress:{
			type: String,
			required: [true, "Seller Address is required."]
		}
	}
	],
	productName:{
		type: String,
		required: [true, "Product name is required."]
	},
	description:{
		type: String,
		required: [true, "Product description is required."]
	},
	quantity:{
		type: Number,
		required: [true, "Product quantity is required."]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	price:{
		type: Number,
		required: [true, "Product price is required."]
	},
	createdOn:{
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", productSchema);