const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	address:{
		type: String,
		required: [true, "Address is required."]
	},
	mobileNo:{
		type: String,
		required: [true, "Mobile number is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	classification:{
		type: String,
		required: [true, "User classification is required."]
		// buyer, seller, admin
	},
	products:[
		{
			productId:{
				type: String,
				required: [true, "Product ID is required."]
			}, 
			createdOn:{
				type: Date,
				default: new Date()
			}
		}
	],
	orders:[
		{
			orderId:{
				type: String,
				required: [true, "Order ID is required"]
			},
			isPaid:{
				type: Boolean,
				default: true	
			},
			status:{
				type: String,
				default: "For Delivery"
			},
			orderedOn:{
				type: Date,
				default: new Date()
			}
		}
	], 
	createdOn:{
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("User", userSchema);