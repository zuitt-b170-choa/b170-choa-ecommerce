const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	buyerId:{
		type: String,
		required: [true, "Buyer ID is required."]
	},
	sellerId:{
		type: String,
		required: [true, "Seller ID is required."]
	},
	productId:{
		type: String,
		required: [true, "Product ID is required."]
	},
	quantity:{
		type: Number,
		required: [true, "Product quantity order is required."]
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	paymentMethod:{
		type: String,
		required: [true, "Payment method is required."]
	},
	deliveryMethod:{
		type: String,
		default: "ShoZada Logistics"
	},
	isPaid:{
		type: Boolean,
		default: true
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date
	}
});

module.exports = mongoose.model("Order", orderSchema);