const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

const app = express();
const port = 4000;

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://gabchoa:gabchoa123@wdc028-course-booking.c9sio.mongodb.net/b170-Choa-Ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});



app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database."));

app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`));


//https://immense-refuge-40846.herokuapp.com/

