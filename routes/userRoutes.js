const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const userController = require("../controllers/userController.js")

router.post("/checkEmail", (req, res)=> {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

//user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result=>res.send(result));
});

// user login (access token)
router.post("/login", (req,res)=>{
	userController.userLogin(req.body).then(result=>res.send(result));
})

/*router.get("/checkout", auth.verify, (req,res)=>{
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		sellerId: req.body.sellerId,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	console.log(data);
	userController.userCheckout(data).then(result=>res.send(result));
})*/

/*
	Business Logic: Set User as Admin
		1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
		2. API validates JWT, return false if validation fails.
		3. If validation successful, API finds user with ID matching the userID URL parameter and sets its isAdmin property to true.
*/

router.get("/details", auth.verify, (req, res) => {
	//decode - decrypts token inside the authorization (w/c is in headers of request)
	//req.headers.authorization contains the token that was creeated for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))	
})



router.put("/:id/setAsAdmin", auth.verify, (req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	/*console.log(data);*/
	userController.setToAdmin(data, req.params).then(result=>res.send(result));
})

router.get("/orders", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getOrders(data).then(result=>res.send(result));
})

router.get("/myOrders", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getMyOrders(data).then(result=>res.send(result));
})

module.exports = router;