const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const productController = require("../controllers/productController.js")
const orderController = require("../controllers/orderController.js")

router.post("/createProduct", auth.verify, (req, res)=>{
	const user = auth.decode(req.headers.authorization);
	productController.createProduct(user, req.body).then(result=>res.send(result))
})
	
router.get("/", (req,res)=>{
	productController.showAllProduct().then(result=>res.send(result));
})

router.get("/activeProducts", (req,res)=>{
	productController.showActiveProduct().then(result=>res.send(result));
})

router.get("/:id", (req,res)=>{
	productController.showProduct(req.params.id).then(result=>res.send(result));
})

router.put("/:id", auth.verify,(req,res)=>{
	const user = auth.decode(req.headers.authorization);
	productController.updateProduct(user, req.params.id ,req.body).then(result=>res.send(result));
})

router.put("/:id/archive", auth.verify,(req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params.id, req.body).then(result=>res.send(result))
})

module.exports = router;