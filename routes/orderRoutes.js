const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const orderController = require("../controllers/orderController.js")

router.post("/checkout", auth.verify, (req,res)=>{
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		sellerId: req.body.sellerId,
		productId: req.body.productId,
		quantity: req.body.quantity,
		paymentMethod: req.body.paymentMethod
	}
	/*console.log(data);*/
	orderController.userCheckout(data).then(result=>res.send(result));
})



module.exports = router;