const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js")
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// user registration

module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				return true
			}else{
				return false
			}
		}
	})
}


module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		address: reqBody.address,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10),
		classification: reqBody.classification
	})
		return newUser.save().then((saved, err)=>{
			if(err){
				return false;
			}
			else{
				return true;
			}
		})
	}

//user login
module.exports.userLogin = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then((result)=>{
		if(result === null){
			return false;
		}else{
			let isPassword = bcrypt.compareSync(reqBody.password, result.password);
			if(isPassword){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				console.log(result);
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if (result === null) {
			return false
		} else {
			return result
		}
	})
}

module.exports.setToAdmin = (data, reqParams)=>{
	let isUserUpdated = {
		isAdmin: true
	}
	if(data.isAdmin === true){
	return User.findByIdAndUpdate(reqParams.id, isUserUpdated).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
	}else{
		return false;
		console.log("Not an admin user.")
	}
}

module.exports.getOrders = (data)=>{
	return User.findById(data.userId).then((result, err)=>{
		if(err){
			return false
		}else if(result.isAdmin === true && result.classification === "admin"){
			return Order.find().then(result=>{
				return result
			})
		}else if(result.isAdmin === true && result.classification === "seller"){
			return Order.find({sellerId: data.userId}).then(result=>{
				return result
			})
		}else if(result.isAdmin === null || result.classification === null){
			return false
		}
	})
}

module.exports.getMyOrders = (data)=>{
	return User.findById(data.userId).then((result, err)=>{
		console.log(result)
		if(result.isAdmin === true){
			return false
		}else if(result.isAdmin === false){
			return Order.find({buyerId: data.userId}).then(result=>{
				console.log(result)
				return result
			})
		}else if(result.isAdmin === null){
			return false
		}
	})
}

/*module.exports.userCheckout = (data)=>{
	return User.findById(data.userId).then((result, err)=>{
		if(result.isAdmin != false){
			return `Not authorized to create order.`
		}else{
			return Product.findById(data.productId).then((result, err)=>{
				let arrayFind = result.sellerDescription.find((find,index)=>{
					console.log(find.sellerId);
					if(find.sellerId !== data.sellerId){
						return false;
					}else{
						let updateOrder = {

						}
					}

				})
			})
		}
	})
}*/

/*{sellerDescription: [{sellerId: reqBody.sellerId}]}*/