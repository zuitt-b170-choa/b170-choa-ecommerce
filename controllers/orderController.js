const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js")
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

module.exports.userCheckout = async (data)=>{
	let isOrderUpdate = await User.findById(data.userId).then((result, err)=>{
		if(result.isAdmin != false){
			return `Not authorized to create order.`
		}else{
			return Product.findById(data.productId).then((result, err)=>{
				let arrayFind = result.sellerDescription.find((find,index)=>{
					/*console.log(result);
					console.log("space")
					console.log(find.sellerId);*/
					if(find.sellerId !== data.sellerId){
						return false;
					}else{
						let newOrder = new Order({
							buyerId: data.userId,
							sellerId: find.sellerId,
							productId: data.productId,
							quantity: data.quantity,
							paymentMethod: data.paymentMethod,
							totalAmount: result.price * data.quantity
						})
						return newOrder.save().then((savedOrder,err)=>{
							if(err){
								return false
							}
							else{
								/*console.log("test")
								console.log(savedOrder)*/
								return User.findById(data.userId).then(user=>{
									/*console.log("test user")
									console.log(user);*/
									user.orders.push({orderId: savedOrder.id})
										return user.save().then((result,err)=>{
											if(err){
												return false;
											}else{
												return `You have successfully ordered.`
												console.log(`You have successfully ordered.`); 
											}
									})
								})
							}
						})
					}

				})
			})
		}
	})
}