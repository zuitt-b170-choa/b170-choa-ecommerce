const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js")
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

module.exports.createProduct = async (user,req)=>{
	let isProductCreated = await User.findById(user.id).then(result=>{
		if(result.classification === "seller" && result.isAdmin === true){
			// variable requestBody to new product in Schema
			let newProduct = new Product({
				sellerDescription: [
				{
					sellerId: result.id,
					sellerAddress: result.address
				}],
				productName: req.productName,
				description: req.description,
				quantity: req.quantity,
				price: req.price
			})
			// saving new Product to 
			return newProduct.save().then((saved, error)=>{
				if(error){
					return false;
				}else{
					return User.findById(user.id).then(user=>{
						user.products.push({productId: saved.id})
						return user.save().then((final,err)=>{
							if(err){
								return false;
							}
							else if(final){
								console.log("Product created successfully")
								return `Product created successfully`
							}
						})
					})
				}
			})
		}else if(result.classification !== "seller" && result.isAdmin !== true){
			console.log("Not authorized to create product.")
			return "Not authorized to create product"
		}
	})
} // end of createProduct function

// get all products
module.exports.showAllProduct = ()=>{
	return Product.find({}).then(result=>{
		return result;
	})
}

//Show all active products
module.exports.showActiveProduct = ()=>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
}

//show a single product by ID
module.exports.showProduct = (productId)=>{
	return Product.findById(productId).then((result,error)=>{
		if(error){
			return false
		}else{
			return result
		}
	})
}

//update product by ID with authorization of admin, and only specific seller description
module.exports.updateProduct = (user, productId, reqBody)=>{
	return User.findById(user.id).then(result=>{
		if(result.isAdmin === true && result.classification === "admin"){
			console.log("3")
			let updateProduct = {
				productName: reqBody.productName,
				description: reqBody.description,
				quantity: reqBody.quantity,
				price: reqBody.price
				
			}
			return Product.findByIdAndUpdate(productId, updateProduct).then((result,error)=>{
				if(error){
					console.log("1 False")
					return false
				}
				else{
					console.log("2 True")
					return true
				}
			})
		}
		else if(result.isAdmin === true && result.classification === "seller"){
			return Product.findById(productId).then((result,error)=>{
				let arrayFind = result.sellerDescription.find((find,index)=>{
					if(find.sellerId === user.id){
						let updateProduct = {
							productName: reqBody.productName,
							description: reqBody.description,
							quantity: reqBody.quantity,
							price: reqBody.price
							
						}
						return Product.findByIdAndUpdate(productId, updateProduct).then((result,error)=>{
							if(error){
								console.log("4 False")
								return false
							}
							else{
								console.log("5 True")
								return `Updated product`
							}
						})
					}else{
						console.log(`Not an authorized user to update`)
						return `Not an authorized user to update`
					}
				})
			})
		}
		else{
			console.log(`Not an authorized admin to update`);
			return false
		}
	})
}

module.exports.archiveProduct = (user, productId, reqBody)=>{
	return User.findById(user.id).then(result=>{
		let archiveProduct = {
				isActive: reqBody.isActive
			}
		if(result.isAdmin === true && result.classification === "admin"){
			return Product.findByIdAndUpdate(productId, archiveProduct).then((result, error)=>{
				if(error){
					return false
				}
				else{
					return true
				}
			})
		} // end of isAdmin and admin classification if statement
		else if(result.isAdmin === true && result.classification === "seller"){
			return Product.findById(productId).then(result=>{
				let arrayProductDescription = result.sellerDescription.find((find)=>{
					if(find.sellerId === user.id){
						return Product.findByIdAndUpdate(productId, archiveProduct).then((save, error)=>{
							if(error){
								return false;
							}
							else{
								return true;
							}
						})
					}
				})
			})
		} // end of specific seller if statment
		else{
			return `not authorized`
		}
	})
}