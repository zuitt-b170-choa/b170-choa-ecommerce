const jwt = require("jsonwebtoken");
const secret = "SecretIsSecret";

module.exports.createAccessToken = (user)=>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data,secret,{});
}

//token verification
module.exports.verify = (req,res,next)=>{ 
	let token = req.headers.authorization;

	if(typeof token != "undefined"){
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				console.log(err)
				return res.send({auth: "fail"});
			}else{
				next();
			}
		});
	}
}

module.exports.decode = (token)=>{
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				console.log(error)
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}